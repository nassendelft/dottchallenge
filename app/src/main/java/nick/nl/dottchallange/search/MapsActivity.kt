package nick.nl.dottchallange.search

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_maps.*
import nick.nl.dottchallange.R
import nick.nl.dottchallange.search.details.DetailsActivity


private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0
private const val DEFAULT_ZOOM = 16f

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var map: GoogleMap
    private lateinit var viewModel: MapViewModel
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private var locationPermissionGranted = false
    private var shouldInitMapPosition = false

    private var lastKnownLocation: Location? = null

    private val defaultLocation = LatLng(52.370216, 4.895168) // Amsterdam

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        (supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment).getMapAsync(this)

        viewModel = ViewModelProviders.of(this).get(MapViewModel::class.java).apply {
            getRestaurants().observe(
                this@MapsActivity,
                Observer<MapViewModel.SearchResponse> { onResponse(it!!) })
        }

        shouldInitMapPosition = savedInstanceState == null
    }

    private fun onResponse(response: MapViewModel.SearchResponse) {
        when (response) {
            is MapViewModel.SearchResponse.Loading -> {
                map.clear()
                loading.visibility = View.VISIBLE
            }
            is MapViewModel.SearchResponse.Success -> {
                addMarkers(response.restaurants)
                loading.visibility = View.GONE
            }
            is MapViewModel.SearchResponse.Fail -> {
                response.error.printStackTrace()
                loading.visibility = View.GONE
                Toast.makeText(this, "Failed getting data", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun addMarkers(items: List<MapModel>) {
        if(!::map.isInitialized) return

        map.clear()
        items.forEach {
            val marker = map.addMarker(
                MarkerOptions()
                    .position(LatLng(it.location.lat, it.location.lng))
                    .title(it.name)
            )
            marker.tag = it.id
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        viewModel.getRestaurants()
            .value?.let { if (it is MapViewModel.SearchResponse.Success) addMarkers(it.restaurants) }
        updateLocationUI()
        getDeviceLocation()

        map.setOnMarkerClickListener {
            DetailsActivity.open(this, it.tag as String)
            true
        }

        map.setOnCameraIdleListener {
            val cameraPosition = map.cameraPosition.target
            if(cameraPosition.latitude == 0.0 && cameraPosition.longitude == 0.0) return@setOnCameraIdleListener
            viewModel.searchRestaurants(cameraPosition.latitude, cameraPosition.longitude)
        }
    }

    private fun updateLocationUI() {
        try {
            if (locationPermissionGranted) {
                map.isMyLocationEnabled = true
            } else {
                map.isMyLocationEnabled = false
                lastKnownLocation = null
                getLocationPermission()
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message)
        }

    }

    private fun getDeviceLocation() {
        try {
            if (locationPermissionGranted) {
                val locationResult = fusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(this) { task ->

                    if (task.isSuccessful && shouldInitMapPosition) {
                        lastKnownLocation = task.result
                        lastKnownLocation?.let {
                            val newLocation = LatLng(it.latitude, it.longitude)
                            map.moveCamera(
                                CameraUpdateFactory.newLatLngZoom(
                                    newLocation,
                                    DEFAULT_ZOOM
                                )
                            )
                            viewModel.searchRestaurants(it.latitude, it.longitude)
                            Log.d("Maps", "Got last known location: ${it.latitude},${it.longitude}")
                        }

                    } else if(!task.isSuccessful) {
                        map.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                defaultLocation,
                                DEFAULT_ZOOM
                            )
                        )
                        map.uiSettings.isMyLocationButtonEnabled = false
                        Log.d("Maps", "Unknown last known location")
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message)
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        locationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true
                }
            }
        }
        updateLocationUI()
    }

    private fun getLocationPermission() {
        val currentPermission = ContextCompat.checkSelfPermission(
            this.applicationContext,
            android.Manifest.permission.ACCESS_FINE_LOCATION
        )

        if (currentPermission == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

}
