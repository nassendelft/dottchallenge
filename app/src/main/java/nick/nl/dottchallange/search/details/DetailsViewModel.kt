package nick.nl.dottchallange.search.details

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.graphics.Color
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import nick.nl.dottchallange.data.GetRestaurantDetailsUseCase
import nick.nl.dottchallange.data.VenueDetails

class DetailsViewModel : ViewModel() {
    private val details = MutableLiveData<DetailsResponse>()

    fun getDetails(): LiveData<DetailsResponse> = details

    @SuppressLint("CheckResult")
    fun getDetails(id: String) {
        val getDetails = GetRestaurantDetailsUseCase.getDetails(id)

        getDetails.map { it.toDetailsModel() }
            .map { DetailsResponse.Success(it) }
            .cast(DetailsResponse::class.java)
            .toObservable()
            .startWith(DetailsResponse.Loading)
            .onErrorReturn { DetailsResponse.Fail(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { response -> details.value = response }
    }

    private fun VenueDetails.toDetailsModel() = DetailsModel(
        name,
        location.address,
        rating.toString(),
        Color.parseColor("#$ratingColor"),
        categories.map {it.name} .reduce { acc: String, cat: String -> "$acc, $cat" },
        description,
        bestPhoto?.let { "${it.prefix}width300${it.suffix}"}
    )

    sealed class DetailsResponse {
        object Loading : DetailsResponse()
        data class Success(val details: DetailsModel) : DetailsResponse()
        data class Fail(val error: Throwable) : DetailsResponse()
    }
}