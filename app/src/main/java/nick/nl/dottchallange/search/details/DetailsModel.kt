package nick.nl.dottchallange.search.details

class DetailsModel(
    val title: String,
    val address: String,
    val rating: String,
    val ratingColor: Int,
    val categories: String,
    val description: String? = null,
    val imageUrl: String? = null
)