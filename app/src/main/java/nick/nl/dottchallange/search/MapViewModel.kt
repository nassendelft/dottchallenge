package nick.nl.dottchallange.search

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import nick.nl.dottchallange.data.FindRestaurantsUseCase
import nick.nl.dottchallange.data.Venue

class MapViewModel : ViewModel() {

    private val restaurants = MutableLiveData<SearchResponse>()

    fun getRestaurants(): LiveData<SearchResponse> = restaurants

    @SuppressLint("CheckResult")
    fun searchRestaurants(lat: Double, lng: Double) {
        // This is just done this way because of time constraints.
        // Ideally this is create by  using a factory passed into the constructor.
        // Obviously the way it is now makes testing this class very difficult.
        val getRestaurants = FindRestaurantsUseCase.getNearbyRestaurants(lat, lng)

        getRestaurants.flatMap { venues -> Observable.fromIterable(venues).map { it.toMapModel() }.toList() }
            .map { SearchResponse.Success(it) }
            .cast(SearchResponse::class.java)
            .toObservable()
            .startWith(SearchResponse.Loading)
            .onErrorReturn { SearchResponse.Fail(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { response -> restaurants.value = response }
    }

    private fun Venue.toMapModel() = MapModel(
        id,
        name,
        Location(location.lat, location.lng)
    )

    sealed class SearchResponse {
        object Loading : SearchResponse()
        data class Success(val restaurants: List<MapModel>) : SearchResponse()
        data class Fail(val error: Throwable) : SearchResponse()
    }
}