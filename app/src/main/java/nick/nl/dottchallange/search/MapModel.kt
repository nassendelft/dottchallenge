package nick.nl.dottchallange.search

data class MapModel(val id: String, val name: String, val location: Location)
data class Location(val lat: Double, val lng: Double)