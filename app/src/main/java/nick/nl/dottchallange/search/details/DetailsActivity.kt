package nick.nl.dottchallange.search.details

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_details.*
import nick.nl.dottchallange.R

class DetailsActivity : AppCompatActivity() {
    private lateinit var viewModel: DetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        viewModel = ViewModelProviders.of(this).get(DetailsViewModel::class.java).apply {
            getDetails().observe(
                this@DetailsActivity,
                Observer<DetailsViewModel.DetailsResponse> { onResponse(it!!) })
        }

        if(viewModel.getDetails().value == null) {
            viewModel.getDetails(intent.getStringExtra(EXTRA_ID))
        }
    }

    private fun onResponse(response: DetailsViewModel.DetailsResponse) {
        when (response) {
            is DetailsViewModel.DetailsResponse.Loading -> {
                content_group.visibility = View.GONE
                loading.visibility = View.VISIBLE
                error.visibility = View.GONE
            }
            is DetailsViewModel.DetailsResponse.Success -> {
                content_group.visibility = View.VISIBLE
                loading.visibility = View.GONE
                error.visibility = View.GONE
                setDetails(response.details)
            }
            is DetailsViewModel.DetailsResponse.Fail -> {
                response.error.printStackTrace()
                content_group.visibility = View.GONE
                loading.visibility = View.GONE
                error.visibility = View.VISIBLE
            }
        }
    }

    private fun setDetails(detailsModel: DetailsModel) {
        name.text = detailsModel.title
        address.text = detailsModel.address
        rating.text = detailsModel.rating
        rating.setTextColor(detailsModel.ratingColor)
        categories.text = detailsModel.categories
        description.text = detailsModel.description
        detailsModel.imageUrl?.let { Glide.with(this).load(it).into(img_main) }
    }

    fun retry(v: View) {
        viewModel.getDetails(intent.getStringExtra(EXTRA_ID))
    }

    companion object {
        private const val EXTRA_ID = "extra_id"

        fun open(context: Context, id: String) {
            val intent = Intent(context, DetailsActivity::class.java)
            intent.putExtra(EXTRA_ID, id)
            context.startActivity(intent)
        }
    }
}
