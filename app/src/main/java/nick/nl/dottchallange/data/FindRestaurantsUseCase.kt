package nick.nl.dottchallange.data

import io.reactivex.Single
import nick.nl.dottchallange.DottChallengeApplication

class FindRestaurantsUseCase(private val fourSquareApi: FourSquareApi) {

    private fun getNearbyRestaurants(lat: Double, lng: Double): Single<List<Venue>> {
        return fourSquareApi.findNearLatLng("$lat,$lng", FourSquareApi.VENUE_CATEGORY_FOOD)
            .map { it.response.venues }
    }

    companion object {
        fun getNearbyRestaurants(lat: Double, lng: Double) =
            FindRestaurantsUseCase(DottChallengeApplication.INSTANCE.fourSquareApi).getNearbyRestaurants(lat, lng)
    }
}
