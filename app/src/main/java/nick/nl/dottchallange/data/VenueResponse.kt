package nick.nl.dottchallange.data

data class VenueResponse(
    val meta: VenueMeta,
    val response: ResponseDetails
)

data class ResponseDetails(
    val venue: VenueDetails
)

data class VenueDetails(
    val attributes: Attributes,
    val beenHere: BeenHere,
    val bestPhoto: BestPhoto? = null,
    val canonicalUrl: String,
    val categories: List<VenueCategory>,
    val contact: Contact,
    val createdAt: Int,
    val description: String,
    val hereNow: HereNow,
    val hours: Hours,
    val id: String,
    val inbox: Inbox,
    val likes: Likes,
    val listed: Listed,
    val location: Location,
    val name: String,
    val page: Page,
    val pageUpdates: PageUpdates,
    val photos: Photos,
    val phrases: List<Phrase>,
    val popular: Popular,
    val rating: Double,
    val ratingColor: String,
    val ratingSignals: Int,
    val shortUrl: String,
    val stats: Stats,
    val storeId: String,
    val timeZone: String,
    val tips: Tips,
    val url: String,
    val venueChains: List<Any>,
    val verified: Boolean
)

data class Inbox(
    val count: Int,
    val items: List<Any>
)

data class Phrase(
    val count: Int,
    val phrase: String,
    val sample: Sample
)

data class Sample(
    val entities: List<Entity>,
    val text: String
)

data class Entity(
    val indices: List<Int>,
    val type: String
)

data class BeenHere(
    val count: Int,
    val lastCheckinExpiredAt: Int,
    val marked: Boolean,
    val unconfirmedCount: Int
)

data class BestPhoto(
    val createdAt: Int,
    val height: Int,
    val id: String,
    val prefix: String,
    val source: Source,
    val suffix: String,
    val visibility: String,
    val width: Int
)

data class Source(
    val name: String,
    val url: String
)

data class Hours(
    val isLocalHoliday: Boolean,
    val isOpen: Boolean,
    val status: String,
    val timeframes: List<Timeframe>
)

data class Timeframe(
    val days: String,
    val includesToday: Boolean,
    val `open`: List<Open>,
    val segments: List<Any>
)

data class Open(
    val renderedTime: String
)

data class VenueCategory(
    val icon: VenueIcon,
    val id: String,
    val name: String,
    val pluralName: String,
    val primary: Boolean,
    val shortName: String
)

data class VenueIcon(
    val prefix: String,
    val suffix: String
)

data class Stats(
    val checkinsCount: Int,
    val tipCount: Int,
    val usersCount: Int,
    val visitsCount: Int
)

data class HereNow(
    val count: Int,
    val groups: List<Group>,
    val summary: String
)

data class Group(
    val count: Int,
    val items: List<Any>,
    val name: String,
    val type: String
)

data class Photos(
    val count: Int,
    val groups: List<GroupX>
)

data class GroupX(
    val count: Int,
    val items: List<Item>,
    val name: String,
    val type: String
)

data class Item(
    val createdAt: Int,
    val height: Int,
    val id: String,
    val prefix: String,
    val source: Source,
    val suffix: String,
    val user: User,
    val visibility: String,
    val width: Int
)

data class User(
    val firstName: String,
    val gender: String,
    val id: String,
    val lastName: String
)

data class Tips(
    val count: Int,
    val groups: List<GroupXX>
)

data class ItemX(
    val agreeCount: Int,
    val authorInteractionType: String,
    val canonicalUrl: String,
    val createdAt: Int,
    val disagreeCount: Int,
    val editedAt: Int,
    val id: String,
    val lang: String,
    val likes: Likes,
    val logView: Boolean,
    val photo: Photo,
    val photourl: String,
    val text: String,
    val todo: Todo,
    val type: String,
    val url: String,
    val user: UserX
)

data class Photo(
    val createdAt: Int,
    val height: Int,
    val id: String,
    val prefix: String,
    val source: Source,
    val suffix: String,
    val visibility: String,
    val width: Int
)

data class Todo(
    val count: Int
)

data class UserX(
    val firstName: String,
    val gender: String,
    val id: String,
    val photo: PhotoX,
    val type: String
)

data class PhotoX(
    val prefix: String,
    val suffix: String
)

data class Likes(
    val count: Int,
    val groups: List<GroupXX>,
    val summary: String
)

data class GroupXX(
    val count: Int,
    val items: List<Any>,
    val type: String
)

data class LikesX(
    val count: Int,
    val summary: String
)

data class PageUpdates(
    val count: Int,
    val items: List<Any>
)

data class Attributes(
    val groups: List<GroupXXX>
)

data class GroupXXX(
    val count: Int,
    val items: List<ItemXX>,
    val name: String,
    val summary: String,
    val type: String
)

data class ItemXX(
    val displayName: String,
    val displayValue: String
)

data class Listed(
    val count: Int,
    val groups: List<Group>
)

data class ItemXXX(
    val canonicalUrl: String,
    val collaborative: Boolean,
    val createdAt: Int,
    val description: String,
    val editable: Boolean,
    val followers: Followers,
    val id: String,
    val listItems: ListItems,
    val name: String,
    val photo: PhotoXX,
    val `public`: Boolean,
    val type: String,
    val updatedAt: Int,
    val url: String,
    val user: UserXX
)

data class Followers(
    val count: Int
)

data class ListItems(
    val count: Int,
    val items: List<ItemXXXX>
)

data class ItemXXXX(
    val createdAt: Int,
    val id: String,
    val photo: PhotoXX
)

data class PhotoXX(
    val createdAt: Int,
    val height: Int,
    val id: String,
    val prefix: String,
    val suffix: String,
    val visibility: String,
    val width: Int
)

data class PhotoXXX(
    val createdAt: Int,
    val height: Int,
    val id: String,
    val prefix: String,
    val suffix: String,
    val user: UserXX,
    val visibility: String,
    val width: Int
)

data class UserXX(
    val firstName: String,
    val gender: String,
    val id: String,
    val lastName: String,
    val photo: Photo
)

data class Location(
    val address: String,
    val cc: String,
    val city: String,
    val country: String,
    val crossStreet: String,
    val formattedAddress: List<String>,
    val lat: Double,
    val lng: Double,
    val postalCode: String,
    val state: String
)

data class Popular(
    val isLocalHoliday: Boolean,
    val isOpen: Boolean,
    val status: String,
    val timeframes: List<TimeframeX>
)

data class TimeframeX(
    val days: String,
    val `open`: List<Open>,
    val segments: List<Any>
)

data class Page(
    val pageInfo: PageInfo,
    val user: UserXXX
)

data class UserXXX(
    val bio: String,
    val contact: Contact,
    val firstName: String,
    val gender: String,
    val homeCity: String,
    val id: String,
    val lists: Lists,
    val photo: Photo,
    val tips: TipsX,
    val type: String
)

data class Lists(
    val groups: List<Group>
)

data class Contact(
    val facebook: String,
    val twitter: String
)

data class TipsX(
    val count: Int
)

data class PageInfo(
    val banner: String,
    val description: String,
    val links: Links
)

data class Links(
    val count: Int,
    val items: List<ItemXXXXX>
)

data class ItemXXXXX(
    val url: String
)

data class ContactX(
    val facebook: String,
    val facebookName: String,
    val facebookUsername: String,
    val formattedPhone: String,
    val instagram: String,
    val phone: String,
    val twitter: String
)

data class VenueMeta(
    val code: Int,
    val requestId: String
)