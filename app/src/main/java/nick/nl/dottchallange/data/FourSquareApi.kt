package nick.nl.dottchallange.data

import io.reactivex.Single
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

private val logging = HttpLoggingInterceptor().run {
    level = HttpLoggingInterceptor.Level.BODY
    return@run this
}

class ClientAndSecretInjector(private val clientId: String, private val clientSecret: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()
            .url(chain.request().url().newBuilder()
                .addQueryParameter("client_id", clientId)
                .addQueryParameter("client_secret", clientSecret)
                .addQueryParameter("v", "20190201")
                .build())
            .build()
        return chain.proceed(request)
    }
}

interface FourSquareApi {

    @GET("/v2/venues/search")
    fun findNearLatLng(
        @Query("ll") latLng: String,
        @Query("categoryId") categoryId: String
    ): Single<SearchResponse>

    @GET("/v2/venues/{id}")
    fun getVenueDetails(@Path("id") id: String): Single<VenueResponse>

    companion object {

        const val VENUE_CATEGORY_FOOD = "4d4b7105d754a06374d81259"

        fun create(clientId: String, clientSecret: String) = Retrofit.Builder()
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(ClientAndSecretInjector(clientId, clientSecret))
                    .addInterceptor(logging)
                    .build()
            )
            .baseUrl("https://api.foursquare.com")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(FourSquareApi::class.java)
    }
}