package nick.nl.dottchallange.data

import io.reactivex.Single
import nick.nl.dottchallange.DottChallengeApplication

class GetRestaurantDetailsUseCase(private val fourSquareApi: FourSquareApi) {

    private fun getNearbyRestaurants(id: String): Single<VenueDetails> {
        return fourSquareApi.getVenueDetails(id).map { it.response.venue }
    }

    companion object {
        fun getDetails(id: String) =
            GetRestaurantDetailsUseCase(DottChallengeApplication.INSTANCE.fourSquareApi).getNearbyRestaurants(id)
    }
}
