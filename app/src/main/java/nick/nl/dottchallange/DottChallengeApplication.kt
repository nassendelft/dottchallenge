package nick.nl.dottchallange

import android.app.Application
import nick.nl.dottchallange.data.FourSquareApi

class DottChallengeApplication : Application() {

    // When a project gets bigger I would probably abstract and put wiring/dependency management
    // in a different way. I.e. using Dagger or Koin
    val fourSquareApi: FourSquareApi by lazy {
        val (id, secret) = getFourSquareValues()
        FourSquareApi.create(id, secret)
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }

    private fun getFourSquareValues() =
        Pair(getString(R.string.foursquare_client_id), getString(R.string.foursquare_client_secret))

    companion object {
        lateinit var INSTANCE: DottChallengeApplication
    }
}